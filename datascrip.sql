-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 11, 2023 at 11:09 AM
-- Server version: 5.7.37-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datascrip`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id_acc` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `deleted` int(2) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id_acc`, `fullname`, `username`, `password`, `deleted`, `created_date`, `updated_date`, `role`) VALUES
(3, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, '2023-01-13 05:44:32', '2023-01-13 05:44:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id_banner` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id_banner`, `filename`, `link`, `created_date`, `update_date`, `update_by`) VALUES
(3, '2301301156518510.jpg', '', '2023-01-30 11:56:51', '2023-01-30 14:16:28', 0),
(4, '2301301156581227.jpg', '', '2023-01-30 11:56:58', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_desktop_banner`
--

CREATE TABLE `home_desktop_banner` (
  `id_banner` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `btn` int(2) NOT NULL,
  `btn_title` varchar(255) NOT NULL,
  `btn_link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_desktop_banner`
--

INSERT INTO `home_desktop_banner` (`id_banner`, `filename`, `link`, `created_date`, `update_date`, `update_by`, `heading`, `caption`, `btn`, `btn_title`, `btn_link`) VALUES
(1, '2306110159513436.png', '', '2023-06-11 13:59:51', '2023-06-11 17:58:49', 3, '360 Virtual Showrooms', 'Jajaran Produk Kami Dalam Jangkauan Anda', 0, 'Jelajahi Sekarang', 'https://google.com'),
(2, '2306110200363523.png', '', '2023-06-11 14:00:36', '2023-06-11 14:04:52', 3, 'Ayo Jelajahi Produk Kami', 'Banyak varian produk yang kami sediakan', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `id_logo` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id_logo`, `filename`) VALUES
(1, '2306111010071871.png');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL,
  `event` varchar(255) NOT NULL,
  `log_date` datetime NOT NULL,
  `log_by` int(2) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_slider`
--

CREATE TABLE `promo_slider` (
  `id_banner` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo_slider`
--

INSERT INTO `promo_slider` (`id_banner`, `filename`, `link`, `created_date`, `update_date`, `update_by`) VALUES
(2, '2306110559164508.png', '', '2023-06-11 17:59:16', '0000-00-00 00:00:00', 0),
(3, '2306110559219843.png', '', '2023-06-11 17:59:21', '0000-00-00 00:00:00', 0),
(4, '2306110559408049.png', '', '2023-06-11 17:59:27', '2023-06-11 17:59:40', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id_acc`);

--
-- Indexes for table `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `home_desktop_banner`
--
ALTER TABLE `home_desktop_banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id_logo`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `promo_slider`
--
ALTER TABLE `promo_slider`
  ADD PRIMARY KEY (`id_banner`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id_acc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_desktop_banner`
--
ALTER TABLE `home_desktop_banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `id_logo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promo_slider`
--
ALTER TABLE `promo_slider`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
